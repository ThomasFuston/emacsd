;;; init.el --- Load my Emacs configuration -*- lexical-binding: t; -*-

;;; Commentary:
;;

;;; Code:
(message "Loading init.el: ~ is %S, user-emacs-directory is %S"
         (expand-file-name "~") user-emacs-directory)

;; Boostrapping ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Put settings set with the build-in customization interface away into
;; a seperate file.
(setq custom-file (locate-user-emacs-file "etc/custom.el"))
(when (file-exists-p custom-file) (load custom-file))

;; Add config folder "elisp" to load paths
(add-to-list 'load-path (expand-file-name "elisp" user-emacs-directory))
(add-to-list 'load-path (expand-file-name "defun" user-emacs-directory))


;; Load configuration ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Core configuration
(require 'init-package)			; Package management
(require 'init-ui)          ; Appearance
(require 'init-base)        ; Default settings

;; General functionality
(require 'init-undo-fu)                 ; Stable undo/redo
(require 'init-vundo)                   ; Visual undo/redo
(require 'init-crux)                    ; Crux Package for more functionality
(require 'init-expand-region)           ; Syntactical region selection
(require 'init-multiple-cursors)        ; Add multiple cursor functionality
(require 'init-move-dup)                ; Move lines/selections aroung
(require 'init-avy)                     ; Navigate around text in a breeze
(require 'init-eyebrowse)               ; Workspaces within Emacs
(require 'init-projectile)              ; Project management

;; Improve completion
(require 'init-vertico)                 ; Vertical completion UI
(require 'init-marginalia)              ; Add annotations to vertico
(require 'init-orderless)               ; Orderless completion style
(require 'init-consult)                 ; Consult is awesome

;; Coding
(require 'init-indent)                  ; Indentations
(require 'init-rainbow-delimiters)      ; Colorful brackets etc.
(require 'init-smart-parens)            ; Paredit-lite for non-lisps
(require 'init-flycheck)                ; On The Fly Syntaxchecker
;;(require 'init-lsp)                     ; LSP testing
(require 'init-corfu)                   ; Completion-at-Point

;; Development
(require 'init-css)                    ; CSS development
(require 'init-html)                   ; HTML Development

;; Write
(require 'init-markdown)               ; Markdown writting


;; Defuns  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(require 'defun-goto-line)              ; Improved goto line
(require 'defun-jump-window)            ; Jump forward and backward


;; Last adjustements ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Reset garbage collection and rever the file-name-handler
(add-hook 'emacs-startup-hook
          (lambda ()
            (setq gc-cons-threshold (* 20 1024 1024)
                  gc-cons-percentage 0.1)
            (setq file-name-handler-alist file-name-handler-alist-original)
            (makunbound 'file-name-handler-alist-original)))

;; Make sure Emacs get the right PATH from shell
(use-package exec-path-from-shell
  :init
  (exec-path-from-shell-initialize))

;; Show Loadtime init.el file
(print-time-since-init "init.el")



(provide 'init)
;;; init.el ends here

;; Local Variables:
;; coding: utf-8
;; byte-compile-warnings: (not free-vars unresolved)
;; End:
