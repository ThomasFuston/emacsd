;;; defun-jump-window.el --- Jump backward -*- lexical-binding: t; -*-

;;; Commentary:
;;
;; Add jump window backward to Emacs functionality

;;; Code:

(use-package jump-window
  :ensure nil
  :bind
  ("M-j" . other-window)
  ("M-J" . other-window-back)
  :init
  (defun other-window-back ()
    "Move the cursor to the previous window."
    (interactive)
    (other-window -1))
  (provide 'jump-window))



(provide 'defun-jump-window)
;;; defun-jump-window.el ends here

;; Local Variables:
;; coding: utf-8
;; byte-compile-warnings: (not free-vars unresolved)
;; End:
