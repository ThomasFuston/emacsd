;;; early-init.el --- Emacs early init -*- lexical-binding: t; -*-

;;; Commentary:
;;
;; Emacs 27 introduced early-init.el, which is run before init.el, before
;; package and UI initialization happens.

;;; Code:


;; Track Startup time ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defconst emacs-start-time (float-time))
(defun print-time-since-init (loc)
  (let* ((now (float-time))
         (elapsed (- now emacs-start-time)))
    (message "Init file %s: time=%.4f sec" loc elapsed)))

(message "Starting emacs... early-init.el")


;; Improve startup time ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Disable package init at startup
(setq package-enable-at-startup nil)

;; Defer garbage collection furthe back in startup process
(setq gc-cons-threshold most-positive-fixnum
      gc-cons-percentage 0.6)

;; Unset file-name-handler
(defvar file-name-handler-alist-original file-name-handler-alist)
(setq file-name-handler-alist nil)

;; If an .el file is newer than its corresponding .elc, load the .el
(setq load-prefer-newer t)

;; Interface startup settings  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(menu-bar-mode -1)     ;; Disable menubar
(tool-bar-mode -1)     ;; Disable toolbar
(scroll-bar-mode -1)   ;; Disable scrollbar
(blink-cursor-mode -1) ;; Disable cursor blink

;; Start with a clean Scratchbuffer
(setq inhibit-startup-message t
      initial-scratch-message nil
      inhibit-splash-screen t)

;; Supress some GUI dialogs right from the get go
(setq use-file-dialog nil
      use-dialog-box nil)
(setq x-gtk-use-system-tooltips nil)


;; Print startup time to message buffer ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(print-time-since-init "early-init.el")



(provide 'early-init)
;;; early-init.el ends here

;; Local Variables:
;; coding: utf-8
;; byte-compile-warnings: (not free-vars unresolved)
;; End:
