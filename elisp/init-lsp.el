;;; init-lsp --- lsp  -*- lexical-binding: t; -*-

;;; Commentary:
;;
;; Setup built-in lsp-mode

;;; Code:

(use-package lsp-mode
  :hook (typescript-mode . lsp)
  :diminish " lsp"
  :init
  (setq lsp-headerline-breadcrumb-enable nil) ;; Don't need file path in my buffer
  (setq lsp-lens-enable nil) ;; Hide clutter (reference and test counts)
  (setq lsp-enable-indentation nil) ;; use clojure-mode indentation
  (setq lsp-eldoc-enable-hover nil) ;; use CIDER eldoc
  (setq lsp-modeline-code-actions-enable nil) ;; Don't clutter modeline
  (setq lsp-modeline-diagnostics-enable nil) ;; Don't clutter modeline, jeez
  (setq lsp-completion-provider :none) ;; Skip company-mode
  (setq lsp-enable-symbol-highlighting nil) ;; Don't highlight current symbol
  (setq lsp-apply-edits-after-file-operations nil) ;; Disable broken lsp feature: https://github.com/clojure-lsp/clojure-lsp/issues/1813
  )

(use-package lsp-ui
  :commands lsp-ui-mode
  :custom
  (custom-set-faces
   '(lsp-ui-doc-header ((t :foreground "#abb2bf" :background "#454c59"))))
  :config
  (setq lsp-ui-doc-header t)
  (setq lsp-ui-doc-include-signature t)
  (setq lsp-ui-doc-border "#e06c75")
  (setq lsp-ui-doc-show-with-cursor t)
  (setq lsp-ui-doc-position 'at-point)
  (setq lsp-ui-sideline-show-symbol t))

(provide 'init-lsp)
;;; init-lsp.el ends here

;; Local Variables:
;; coding: utf-8
;; byte-compile-warnings: (not free-vars unresolved)
;; End:
