;;; init-css --- CSS  -*- lexical-binding: t; -*-

;;; Commentary:
;;
;; Setup CSS Development

;;; Code:

(use-package css-mode
  :ensure nil
  :custom
  (css-indent-offset 2))

;; SASS support
(use-package sass-mode)

;; SCSS support
(use-package scss-mode
  :init (setq-default scss-compile-at-save nil))

;; LESS support
(use-package less-css-mode)



(provide 'init-css)
;;; init-css.el ends here

;; Local Variables:
;; coding: utf-8
;; byte-compile-warnings: (not free-vars unresolved)
;; End:
