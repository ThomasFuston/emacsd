;;; init-undo-fu.el --- undo/redo -*- lexical-binding: t; -*-

;;; Commentary:
;;
;; Simple and stable linear undo with redo for Emacs.

;;; Code:


(use-package undo-fu
  :custom
  (undo-limit 400000)           ; 400kb (default is 160kb)
  (undo-strong-limit 3000000)   ; 3mb   (default is 240kb)
  (undo-outer-limit 48000000)   ; 48mb  (default is 24mb)
  :bind
  ("M-u" . undo-fu-only-undo)
  ("M-U" . undo-fu-only-redo)
  )



(provide 'init-undo-fu)
;;; init-undo-fu.el ends here

;; Local Variables:
;; coding: utf-8
;; byte-compile-warnings: (not free-vars unresolved)
;; End:
