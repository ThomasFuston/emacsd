;;; init-rainbow-delimiters.el --- colorful delimiters -*- lexical-binding: t; -*-

;;; Commentary:
;;
;; Highlight corresponding delimiters in the same colors for better
;; readability.

;;; Code:

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))


(provide 'init-rainbow-delimiters)
;;; init-rainbow-delimiters.el ends here

;; Local Variables:
;; coding: utf-8
;; byte-compile-warnings: (not free-vars unresolved)
;; End:
