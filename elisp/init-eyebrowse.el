;;; init-eyebrowse.el --- Workspaces -*- lexical-binding: t; -*-

;;; Commentary:
;;
;; Add Workspace / Layout functionaltiy to Emacs.

;;; Code:

(use-package eyebrowse
  :demand
  :diminish eyebrowse-mode
  :bind
  ("C-c 1" . eyebrowse-switch-to-window-config-1)
  ("C-c 2" . eyebrowse-switch-to-window-config-2)
  ("C-c 3" . eyebrowse-switch-to-window-config-3)
  ("C-c 4" . eyebrowse-switch-to-window-config-4)
  ("C-c q" . eyebrowse-close-window-config)
  :config
  (eyebrowse-mode t))



(provide 'init-eyebrowse)
;;; init-eyebrowse.el ends here

;; Local Variables:
;; coding: utf-8
;; byte-compile-warnings: (not free-vars unresolved)
;; End:
