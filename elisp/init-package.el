;;; init-package.el --- Package management -*- lexical-binding: t; -*-

;;; Commentary:
;;
;; Setup repositories and use-package which is mainly used
;; to create a neat configuration.

;;; Code:


;; Adding Package repositories
(require 'package)
(add-to-list 'package-archives '("nongnu" . "https://elpa.nongnu.org/nongnu/"))
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(add-to-list 'package-archives '("gnu" . "https://elpa.gnu.org/packages/"))
(package-initialize)

;; Use package settings
(use-package use-package
  :demand
  :custom
  (use-package-always-defer t)     ; Always lazy load until ':demand' is set
  (use-package-always-ensure t)    ; Always install without the need of ':ensure'
  (native-comp-async-report-warnings-errors nil)) ; No native compilation warnings

;; Add macro for keybindings
(use-package bind-key :demand)

;; Add macro for keychords
(use-package use-package-chords
  :demand
  :custom
  (key-chord-safety-interval-forward 0.1) ; Minimize Delay
  :config (key-chord-mode 1))

;; Add macro for hydras
(use-package hydra
  :demand
  :commands defhydra)

(use-package use-package-hydra :demand)



(provide 'init-package)
;;; init-package.el ends here
