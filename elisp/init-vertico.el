;;; init-vertico.el --- Vertico -*- lexical-binding: t; -*-

;;; Commentary:
;;
;; Vertico provided a minimalistic vertical completion UI based on the
;; default completion system.

;;; Code:

(use-package vertico
  :demand
  :custom
  (vertico-count 13)                    ; Show max 13 candidates at same time
  (vertico-cycle t)                     ; Go from last to first candidate
  (vertico-resize t)                    ; Do Resize the Minibuffer
  :init
  (vertico-mode)                        ; Enable vertico
  (vertico-reverse-mode))               ; Vertico reversed UI (mainly like Selectrum)



(provide 'init-vertico)
;;; init-vertico.el ends here

;; Local Variables:
;; coding: utf-8
;; byte-compile-warnings: (not free-vars unresolved)
;; End:
