;;; init-ui.el --- User interface -*- lexical-binding: t; -*-

;;; Commentary:
;;
;; This file will improve the looks & feel of the Emacs
;; interface.

;;; Code:

;; Include entire file path in title
(setq frame-title-format '(buffer-file-name "%f" ("%b")))

;; Disable Gnu Splashscreen and *scratch*-buffer message.
(setq inhibit-startup-message t
      initial-scratch-message nil)

;; Color all language features
(setq font-lock-maximum-decoration t)

;; Enable Smooth Scrolling
(setq pixel-scroll-precision-mode t)

;; Always show current key-sequence in minibuffer.
(setq echo-keystrokes 0.02)

;; Always show current line and column of Cursor.
(setq-default line-number-mode t
              column-number-mode t)

;; Highlight the current cursor line but only in the active buffer.
(setq hl-line-sticky-flag nil
      global-hl-line-sticky-flag nil)
(global-hl-line-mode)

;; Highlight matching delimiters
(show-paren-mode)
(setq show-paren-delay 0.1
      show-paren-highlight-openparen t
      show-paren-when-point-inside-paren t
      show-paren-when-point-in-periphery t)

;; Use 'y' or 'n' instead of 'yes' or 'no'.
(defalias 'yes-or-no-p 'y-or-n-p)
(setq confirm-kill-emacs 'y-or-n-p)

;; Font Settings
(set-face-attribute 'default nil
     :family "JetBrains Mono"
     :height 105
     :weight 'normal
     :width 'normal)

(set-face-attribute 'variable-pitch nil
     :family "JetBrains Mono"
     :height 105
     :weight 'normal
     :width 'normal)

(set-face-attribute 'fixed-pitch nil
     :family "JetBrains Mono"
     :height 105
     :weight 'normal
     :width 'normal)

(set-face-attribute 'font-lock-comment-face nil :slant 'italic)
(set-face-attribute 'font-lock-function-name-face nil :slant 'italic)
(set-face-attribute 'font-lock-variable-name-face nil :slant 'italic)
(set-face-attribute 'font-lock-keyword-face nil :slant 'italic)

;; Lets add Colors for light and dark themes
(use-package spacemacs-theme
  :bind
  ("C-x t d" . dark-theme)
  ("C-x t l" . light-theme)
  :init
  (defun light-theme ()
    "Activate light colortheme"
    (interactive)
    (load-theme 'spacemacs-light)
    (spaceline-compile))

  (defun dark-theme ()
    "Activate dark colortheme"
    (interactive)
    (load-theme 'spacemacs-dark)
    (spaceline-compile))

  ;; Load theme
  (load-theme 'spacemacs-dark))


;; Add a fancier modeline
(use-package spaceline
  :demand t
  :custom
  (powerline-height 21)
  (powerline-default-separator 'bar)
  :config
  (require 'spaceline-config)
  (spaceline-emacs-theme))


;; Add diminish to hide some modes in modeline
(use-package diminish :demand t)



(provide 'init-ui)
;;; init-ui.el ends here
