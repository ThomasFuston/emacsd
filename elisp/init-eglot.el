;;; init-eglot --- lsp  -*- lexical-binding: t; -*-

;;; Commentary:
;;
;; Setup built-in lsp-mode

;;; Code:

(use-package eglot
  :ensure nil
  :commands eglot
  :config
  (add-to-list 'eglot-server-programs
               '(typescript-mode . ("ngserver" "--stdio" "--tsProbeLocations" "/home/thomas/.nvm/versions/node/v20.13.1/lib/node_modules
" "--ngProbeLocations" "/home/thomas/.nvm/versions/node/v20.13.1/lib/node_modules

")))
  :hook
  (css-mode . eglot-ensure)
  (mhtml-mode . eglot-ensure)
  (typescript-mode . eglot-ensure)
  ;;  (clojurescript-mode . eglot-ensure)
  ;;  (clojure-mode . eglot-ensure)
  )

;; Make eglot work with flycheck
(use-package flycheck-eglot
  :after (flycheck eglot)
  :config
  (global-flycheck-eglot-mode 1))


(provide 'init-eglot)
;;; init-eglot.el ends here

;; Local Variables:
;; coding: utf-8
;; byte-compile-warnings: (not free-vars unresolved)
;; End:
