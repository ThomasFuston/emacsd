;;; init-projectile.el --- Project management -*- lexical-binding: t -*-

;;; Commentary:
;;
;; Use projectile to add project management functionality to Emacs

;;; Code:

(use-package projectile
  :diminish
  :chords
  ("jj" . hydra-projects/body)
  :hydra (hydra-projects (:color blue)
                         "Project Management"
                         ("f" projectile-find-file "file")
                         ("d" projectile-find-dir "directory")
                         ("b" projectile-switch-to-buffer "buffer")
                         ("c" projectile-find-regexp "code")
                         ("p" projectile-switch-project "project"))
  :config
  (setq projectile-indexing-method 'native)
  (setq projectile-enable-caching t)
  (setq projectile-sort-order 'recentf)
  (projectile-mode +1))



(provide 'init-projectile)
;;; init-projectile.el ends here

;; Local Variables:
;; coding: utf-8
;; byte-compile-warnings: (not free-vars unresolved)
;; End:
