;;; init-marginalia.el --- Margenalia -*- lexical-binding: t; -*-

;;; Commentary:
;;
;; Marginalia is adding helpful 'annotations' of the lists presented
;; in the minibuffer by vertico.

;;; Code:

;; Add annotations
(use-package marginalia
  :demand
  :after vertico
  :custom
  (marginalia-align 'right)             ; Alignment of information
  (marginalia-max-relative-age 0)
  :init
  (marginalia-mode))



(provide 'init-marginalia)
;;; init-marginalia.el ends here

;; Local Variables:
;; coding: utf-8
;; byte-compile-warnings: (not free-vars unresolved)
;; End:
