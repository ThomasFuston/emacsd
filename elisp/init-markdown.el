;;; init-markdown.el --- Markdown -*- lexical-binding: t; -*-

;;; Commentary:
;;
;; Setup Markdown mode

;;; Code:

(use-package markdown-mode
  :defer t
  :custom
  (markdown-fontify-code-blocks-natively t)
  :init
  (add-hook 'markdown-mode 'auto-fill-mode))



(provide 'init-markdown)
;;; init-markdown.el ends here
