;;; init-indent.el --- Better identation  -*- lexical-binding: t; -*-

;;; Commentary:
;;
;;  Add identations helper.

;;; Code:

;; Keep everything always indentde
(use-package aggressive-indent
  :demand
  :diminish
  :hook (after-init . global-aggressive-indent-mode))



(provide 'init-indent)
;;; init-indent.el ends here

;; Local Variables:
;; coding: utf-8
;; byte-compile-warnings: (not free-vars unresolved)
;; End:
