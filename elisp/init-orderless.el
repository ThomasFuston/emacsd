;;; init-orderless.el --- Orderless -*- lexical-binding: t; -*-

;;; Commentary:
;;
;; Provide an 'orderless' completion style that divides the pattern
;; into space-separated components.

;;; Code:

(use-package orderless
  :custom
  (orderless-matching-styles
   '(orderless-literal                  ; Search is a literal string
     orderless-prefixes                 ; The Search is split at word endings
     orderless-initialism               ; The search Should appear as the beginning of a word
     orderless-regexp                   ; If the component is not a valid regexp, it is ignored.
     orderless-flex                     ; Basically fuzzy finding
     ))
  (completion-styles '(orderless basic))
  (completion-category-overrides '((file (styles basic partial-completion))))
  )



(provide 'init-orderless)
;;; init-orderless.el ends here

;; Local Variables:
;; coding: utf-8
;; byte-compile-warnings: (not free-vars unresolved)
;; End:
