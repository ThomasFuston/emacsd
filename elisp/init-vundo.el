;;; init-vundo.el --- visual undo/redo -*- lexical-binding: t; -*-

;;; Commentary:
;;
;; Add visual representation of undo/redo

;;; Code:

(use-package vundo
  :config
  (setq vundo-glyph-alist vundo-unicode-symbols)
  (setq vundo-compact-display t)
  :chords
  ("uu" . vundo)
  )



(provide 'init-vundo)
;;; init-vundo.el ends here

;; Local Variables:
;; coding: utf-8
;; byte-compile-warnings: (not free-vars unresolved)
;; End:
