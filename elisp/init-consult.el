;;; init-consult.el --- Consult -*- lexical-binding: t; -*-

;;; Commentary:
;;
;;  Consult setup

;;; Code:

(use-package consult
  :bind
  (("C-x b" . consult-buffer)
   ("C-s" . consult-line)
   ("C-r" . consult-ripgrep)))

;;(use-package consult-flycheck
  ;;:diminish)



(provide 'init-consult)
;;; init-consult.el ends here

;; Local Variables:
;; coding: utf-8
;; byte-compile-warnings: (not free-vars unresolved)
;; End:
