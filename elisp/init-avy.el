;;; init-avy.el --- Fast navigation -*- lexical-binding: t -*-

;;; Commentary:
;;
;; The Avy package allows to move lighting fast trough files, this combined
;; with keychords, its just awsome!

;;; Code:


(use-package avy
  :demand
  :custom
  (avy-all-windows nil)
  (avy-all-windows-alt t)
  :chords
  (("jl" . avy-goto-line)
   ("jk" . avy-goto-char)))



(provide 'init-avy)
;;; init-avy.el ends here

;; Local Variables:
;; coding: utf-8
;; byte-compile-warnings: (not free-vars unresolved)
;; End:
