;;; init-multiple-cursors.el --- Multiple cursors -*- lexical-binding: t; -*-

;;; Commentary:
;;
;; Adding multiple cursors function.

;;; Code:


(use-package multiple-cursors
  :demand
  :bind
  ("C-c m" . mc/mark-all-like-this)
  ("M-m" . mc/mark-next-like-this)
  ("M-M" . mc/mark-previous-like-this))



(provide 'init-multiple-cursors)
;;; init-multiple-cursors.el ends here

;; Local Variables:
;; coding: utf-8
;; byte-compile-warnings: (not free-vars unresolved)
;; End:
