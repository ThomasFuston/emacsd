;;; init-expand.el --- region selection -*- lexical-binding: t; -*-

;;; Commentary:
;;
;;  This will add syntactical region selection

;;; Code:

(use-package expand-region
  :bind ("M-i" . er/expand-region))



(provide 'init-expand-region)
;;; init-expand-region.el ends here

;; Local Variables:
;; coding: utf-8
;; byte-compile-warnings: (not free-vars unresolved)
;; End:
