;;; init-html --- html  -*- lexical-binding: t; -*-

;;; Commentary:
;;
;; Setup HTML Development

;;; Code:


;; TODO:Tryout tagedit!

(use-package html-mode
  :ensure nil
  :bind (:map html-mode-map
              ("C-c 1" . nil)
              ("C-c 2" . nil)
              ("C-c 3" . nil)
              ("C-c 4" . nil)
              ("C-c 5" . nil)
              ("C-c 6" . nil)))

;; Emmet mode
(use-package emmet-mode
  :diminish
  :hook (html-mode . emmet-mode)
  :init
  (setq emmet-move-cursor-between-quotes t))



(provide 'init-html)
;;; init-html.el ends here
