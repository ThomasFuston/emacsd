;;; init-crux.el --- Extra functionality  -*- lexical-binding: t; -*-

;;; Commentary:
;;
;; Adding more convinient functionality to Emacs.

;;; Code:

(use-package crux
  :bind
  ("C-k" . crux-smart-kill-line)
  ("M-o" . crux-smart-open-line)
  ("M-O" . crux-smart-open-line-above)
  ("M-D" . crux-duplicate-current-line-or-region)
  ("C-x C-a" . crux-kill-other-buffers)
  ("C-x C-i" . crux-cleanup-buffer-or-region)
  ("C-x C-k" . crux-delete-file-and-buffer)
  ("C-x C-r" . crux-rename-file-and-buffer)
  )



(provide 'init-crux)
;;; init-crux.el ends here

;; Local Variables:
;; coding: utf-8
;; byte-compile-warnings: (not free-vars unresolved)
;; End:
